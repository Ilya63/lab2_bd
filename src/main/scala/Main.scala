import org.apache.spark.rdd.RDD

import scala.reflect.ClassTag
import java.time.{Duration, LocalDateTime}

import org.apache.log4j.{Level, Logger}
import org.apache.spark._
import org.apache.spark.sql.SparkSession


object Main {
  def main(args: Array[String]) {
    Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
    Logger.getLogger("org.spark-project").setLevel(Level.WARN)

    val cfg = new SparkConf().setAppName("Test").setMaster("local[2]")
    val sc = new SparkContext(cfg)

    implicit class MyRDDFunctions[T](self: RDD[T])(implicit et: ClassTag[T]) {
      def debug_subset():RDD[T] = {
        self.mapPartitions(_.take(1000))
      }
    }

    def parsXMLPosts(elem: scala.xml.Elem) = {
      val date = elem.attribute("CreationDate")
      val tags = elem.attribute("Tags")
      (date, tags)
    }
    def parseCreationDateAndTags(e:(String, String)) = {
      val (creationDate, tags) = e
      val year = creationDate.substring(0, 4)
      val tagsArray = tags.substring(4, tags.length-4).split("&gt;&lt;")
      (year, tagsArray)
    }

    val years = 2010 to 2020 map(_.toString)
    val topCount = 10
    val posts_path = "file:///D:/data/posts_sample.xml"
    val programming_languages_path = "file:///D:/data/programming-languages.csv"
    val startTime = LocalDateTime.now()

    // создание массива языков
    val programmingLanguages = sc.textFile(programming_languages_path).zipWithIndex
      .filter{case (row, idx) => idx > 1}.map{case (row, idx) => row}
      .map{row => row.split(",")}
      .filter{rowValues => rowValues.size==2}
      .map{
        rowValues => val Seq(name, link) = rowValues.toSeq
        name.toLowerCase
      }.collect()

    // парсинг posts
    val postsRDD = sc.textFile(posts_path)
    val posts_count = postsRDD.count
    val yearLanguageTags = postsRDD.zipWithIndex.filter{case (s, idx) => idx > 2 && idx < posts_count - 1}
      .map(row => row._1)
      .map(row => scala.xml.XML.loadString(row))
      .map(parsXMLPosts)
      .filter {x => x._1.isDefined && x._2.isDefined}
      .map{x => (x._1.mkString, x._2.mkString)}
      .map(parseCreationDateAndTags)
      .flatMap{ case (year, tags) => tags.map(tag => (year, tag))}
      .filter{ case (year, tag) => programmingLanguages.contains(tag) }.cache()
    val countsByYears = years
      .map{reportYear => yearLanguageTags.filter{case (tagYear, tag) => reportYear==tagYear}
      .map{ case (tagYear, tag) => (tag, 1)}
      .reduceByKey{(a, b) => a + b}
      .map{ case (tag, count) => (reportYear, tag, count)}
      }
    val sortYearsCounts = countsByYears.map{ countsByYears =>
      countsByYears.sortBy{ case (year, tag, count) => -count }.take(topCount)
    }
    val finalResult = sortYearsCounts.reduce((a, b) => a.union(b))


    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    finalResult.take(5).foreach(println)
    val finalDF = sc.parallelize(finalResult).toDF("Year", "Language", "Count")
    val stopTime = LocalDateTime.now()

    finalDF.show(years.size*topCount)
    println(s"duration ${ java.time.Duration.between(startTime, stopTime) }" )

    // запись в parquet и вывод результата
    finalDF.write.parquet("file:///D:/data/finalDFtest.parquet")
    val spark = SparkSession
      .builder()
      .appName("read parquet")
      .config("spark.some.config.option", "some-value")
      .getOrCreate()
    val parquetFileDF = spark.read.parquet("file:///D:/data/finalDFtest.parquet")
    //parquetFileDF.show()
    parquetFileDF.createOrReplaceTempView("parquetFile")
    val topDF = spark.sql("SELECT * FROM parquetFile")
    topDF.show(120)
    sc.stop()

  }
}
