name := "Lab2_DB"

version := "0.1"

scalaVersion := "2.12.9"

logLevel := Level.Error

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.0",
  "org.apache.spark" %% "spark-sql" % "2.4.0",
  "org.apache.spark" % "spark-mllib_2.4" % "1.1.0"
)
